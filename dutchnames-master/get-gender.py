import sys

female_username = open("female_output.txt", "wt")
male_username = open("male_output.txt", "w")

female=[name.strip() for name in open("names/nl-female.txt").readlines()]
male=[name.strip() for name in open("names/nl-male.txt").readlines()]

for line in sys.stdin:
    name = line.strip().split("\t")[0].lower() # assumes name is first in tab-separated input
    f=0
    m=0
    for n in female:
        if n in name:
            f+=1
    for n in male:
        if n in name:
            m+=1

    if f > m:
        female_username.write(line.strip() + ",") # writes the names of females into a file seperated by commas.
    elif m > f:
        male_username.write(line.strip() + ",") # writes the names of females into a file seperated by commas.
    else:
        pass #not found
