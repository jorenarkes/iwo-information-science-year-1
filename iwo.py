#!/usr/bin/python3
# File name: iwo.py
# This is a script for the course: Introduction to Scientific Research.
# Author: Joren Arkes, S3545881

import gzip
import json
import re
import glob

def main():

    """
    This line determines in what directory im working. Since I work in different directories, I change the path in the file_list variable to one of the following:
    /2017/02/20170225.*.out.gz
    /2017/02/20170226.*.out.gz
    /2017/02/20170227.*.out.gz
    /2017/02/20170228.*.out.gz
    /2018/02/20180225.*.out.gz
    /2018/02/20180226.*.out.gz
    /2018/02/20180227.*.out.gz
    /2018/02/20180228.*.out.gz
    Depending on which files I want to look at, I change it manually.
    """
    file_list = glob.glob('/net/corpora/twitter2/Tweets/2018/02/20180228:*.out.gz')

    female_username = open("dutchnames-master/female_output.txt", "rt")
    male_username = open("dutchnames-master/male_output.txt", "rt")

    for line in female_username:
        female_users = line.split(",")
    for line in male_username:
        male_users = line.split(",")


    female_wollah_counter = female_fissa_counter = female_fattoe_counter = female_pattas_counter = 0
    male_wollah_counter = male_fissa_counter = male_fattoe_counter = male_pattas_counter = 0


    # This counter is here to give feedback about the file looping process and will be updated at the bottom of the first next loop
    file_counter = 0


    """Opens all files that are in the previously determined directory in turn and runs the whole script over each file at a time"""
    for tweet_file in file_list:
        with gzip.open(tweet_file, 'rt', encoding='UTF-8') as f:

            data = f.readlines()
            male_tweets = []
            female_tweets = []
            for line in data:
                dictionary = json.loads(line)
                user = dictionary.get("user")
                if user.get("screen_name") in female_users:
                    female_tweet = dictionary.get("text")
                    sub_female = re.sub('(https.+[ \n]?|[\U00010000-\U0010ffff])+', '', female_tweet)#removes all links and emoticons
                    try:#Removes hashtags if hastag at end of string
                        pattern = re.compile('.+?(?=((#[^ \n]*[ \n]?)*$))')
                        matches_female = pattern.search(sub_female)
                        match_female = matches_female.group()
                        female_tweets.append(match_female)
                    except AttributeError:#adds to list if no hashtags at end of string
                        female_tweets.append(sub_female)
                elif user.get("screen_name") in male_users:
                    male_tweet = dictionary.get("text")
                    sub_male = re.sub('(https.+[ \n]?|[\U00010000-\U0010ffff])+', '', male_tweet)#removes all links and emoticons
                    try:#Removes hashtags if hastag at end of string
                        pattern = re.compile('.+?(?=((#[^ \n]*[ \n]?)*$))')
                        matches_male = pattern.search(sub_male)
                        match_male = matches_male.group()
                        male_tweets.append(match_male)
                    except AttributeError:#adds to list if no hashtags at end of string
                        male_tweets.append(sub_male)
                else:
                    pass

            female_cleaned = []
            for line in female_tweets:
                end_clean = re.sub(r'[\W\s]*$', '', line)#removes all non-alphanumeric and whitespace characters at the end of string
                clean_line = re.sub(r'\s', ' ', end_clean)#replaces double or more whitespace with a single space
                female_cleaned.append(clean_line)#makes the list with cleaned tweets

            male_cleaned = []
            for line in male_tweets:
                end_clean = re.sub(r'[\W\s]*$', '', line)#removes all non-alphanumeric and whitespace characters at the end of string
                clean_line = re.sub(r'\s', ' ', end_clean)#replaces double or more whitespace with a single space
                male_cleaned.append(clean_line)#makes the list with cleaned tweets


            for sent_female in female_cleaned:
                words_female = sent_female.split(' ')
                for word_female in words_female:
                    if 'wollah' in word_female:
                        female_wollah_counter += 1
                    elif 'fissa' in word_female:
                        female_fissa_counter += 1
                    elif 'fattoe' in word_female:
                        female_fattoe_counter += 1
                    elif 'pattas' in word_female:
                        female_pattas_counter += 1

            for sent_male in male_cleaned:
                words_male = sent_male.split(' ')
                for word_male in words_male:
                    if 'wollah' in word_male:
                        male_wollah_counter += 1
                    elif 'fissa' in word_male:
                        male_fissa_counter += 1
                    elif 'fattoe' in word_male:
                        male_fattoe_counter += 1
                    elif 'pattas' in word_male:
                        male_pattas_counter += 1

        file_counter += 1
        print(file_counter)


    print("female wollah:\t", female_wollah_counter)
    print("female fissa:\t", female_fissa_counter)
    print("female fattoe:\t", female_fattoe_counter)
    print("female pattas:\t", female_pattas_counter)
    print("male wollah:\t", male_wollah_counter)
    print("male fissa:\t", male_fissa_counter)
    print("male fattoe:\t", male_fattoe_counter)
    print("male pattas:\t", male_pattas_counter)


if __name__ == '__main__':
    main()

